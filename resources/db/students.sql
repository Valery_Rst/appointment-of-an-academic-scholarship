-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.3.13-MariaDB-log - mariadb.org binary distribution
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп данных таблицы students.about: ~14 rows (приблизительно)
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` (`id`, `student_id`, `is_bad_gray`, `is_active_scholarship`) VALUES
	(1, 1, 0, 1),
	(2, 2, 1, 0),
	(3, 3, 1, 0),
	(4, 4, 0, 1),
	(5, 5, 0, 1),
	(6, 6, 1, 0),
	(8, 8, 0, 1),
	(9, 9, 1, 0),
	(10, 10, 1, 0),
	(11, 11, 1, 0),
	(12, 12, 0, 0),
	(13, 13, 1, 0),
	(14, 14, 1, 0),
	(15, 15, 0, 1);
/*!40000 ALTER TABLE `about` ENABLE KEYS */;

-- Дамп данных таблицы students.scholarship: ~12 rows (приблизительно)
/*!40000 ALTER TABLE `scholarship` DISABLE KEYS */;
INSERT INTO `scholarship` (`id`, `student_id`, `amount`) VALUES
	(1, 1, 750),
	(2, 2, 0),
	(3, 3, 0),
	(4, 4, 750),
	(5, 5, 750),
	(6, 6, 0),
	(8, 8, 750),
	(9, 9, 0),
	(10, 10, 0),
	(11, 11, 0),
	(12, 12, 0),
	(13, 13, 0),
	(14, 14, 0),
	(15, 15, 750);
/*!40000 ALTER TABLE `scholarship` ENABLE KEYS */;

-- Дамп данных таблицы students.student: ~15 rows (приблизительно)
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `name`, `surname`) VALUES
	(1, 'Албадаева', 'Анастасия'),
	(2, 'Андрюшин', 'Дмитрий'),
	(3, 'Воеводин', 'Александр'),
	(4, 'Волков', 'Валерий'),
	(5, 'Горбачёва', 'Ольга'),
	(6, 'Горшков', 'Никита'),
	(7, 'Гусаров', 'Евгений'),
	(8, 'Испольнов', 'Кирилл'),
	(9, 'Кожевников', 'Дмитрий'),
	(10, 'Комисарова', 'Маргарита'),
	(11, 'Куликов', 'Михаил'),
	(12, 'Проничкин', 'Вячеслав'),
	(13, 'Стрыгин', 'Михаил'),
	(14, 'Челноков', 'Егор'),
	(15, 'Шишкова', 'Дарья');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
