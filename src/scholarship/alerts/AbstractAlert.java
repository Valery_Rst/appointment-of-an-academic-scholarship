package scholarship.alerts;

import javafx.scene.control.Alert;

public abstract class AbstractAlert {
    private Alert alert;

    AbstractAlert(Alert.AlertType alertType, String content) {
        this.alert = new Alert(alertType);
        this.alert.setContentText(content);
    }

    public void showAlert() {
        this.alert.showAndWait();
    }
}