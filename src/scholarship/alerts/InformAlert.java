package scholarship.alerts;

import javafx.scene.control.Alert;

public class InformAlert extends AbstractAlert {
    public InformAlert(String content) {
        super(Alert.AlertType.INFORMATION, content);
    }
}