package scholarship.alerts;

import javafx.scene.control.Alert;

public class WarningAlert extends AbstractAlert{
    public WarningAlert(String content) {
        super(Alert.AlertType.WARNING, content);
    }
}