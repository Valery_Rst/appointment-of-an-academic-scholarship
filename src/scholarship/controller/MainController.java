package scholarship.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import scholarship.alerts.InformAlert;
import scholarship.alerts.WarningAlert;
import scholarship.data.ScholarshipTable;
import scholarship.data.StudentDataTable;
import scholarship.data.StudentTable;
import scholarship.model.Student;
import scholarship.validator.AssigningScholarshipRule;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private ObservableList<Student> studentList;
    @FXML   private ListView<Student> students;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.studentList = FXCollections.observableArrayList();
        this.students.setItems(studentList);
        setObservableStudentList();
    }

    @FXML
    private void assignScholarship() {
        if (getSelectedListViewIndex() == -1) {
            return;
        }
        Student currentStudent = students.getSelectionModel().getSelectedItem();
        AssigningScholarshipRule rule = new AssigningScholarshipRule(currentStudent);
        if (!rule.getInvalidText().isEmpty()) {
            new WarningAlert(rule.getInvalidText()).showAlert();
            return;
        }

        if (rule.getInvalidText().isEmpty()) {
            new ScholarshipTable().assignScholarship(currentStudent);
            new InformAlert("Стипендия назначена!").showAlert();
            updateObservableStudentList();
        }
    }

    private void setObservableStudentList() {
        List<Student> studentList = new StudentTable().getStudentList();
        studentList.forEach(student -> student.setScholarship(new ScholarshipTable().getScholarshipById(student)));
        studentList.forEach(student -> student.setStudentData(new StudentDataTable().getStudentDataById(student)));
        this.studentList.addAll(studentList);
    }
    private void updateObservableStudentList() {
        this.studentList.clear();
        setObservableStudentList();
    }
    private int getSelectedListViewIndex() {
        return this.students.getSelectionModel().getSelectedIndex();
    }
}