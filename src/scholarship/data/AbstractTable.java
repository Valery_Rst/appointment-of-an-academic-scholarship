package scholarship.data;

import java.sql.*;

public abstract class AbstractTable {
    protected Connection connection;
    protected PreparedStatement preparedStatement;
    protected ResultSet resultSet;
    protected Statement statement;

    protected AbstractTable() {
        this.connection = DataBaseConnector.getInstance().getConnection();
    }

    public void connectionClose() {
        DataBaseConnector.getInstance().closeConnection(this.connection);
    }
    public void preparedStatementClose() {
        try {
            this.preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void resultSetClose() {
        try {
            this.resultSet.close();
        } catch (SQLException e) {
            e.getCause();
        }
    }
    public void statementClose() {
        try {
            this.statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void setResultSetBySql(String where) {
        try {
            this.statement = this.connection.createStatement();
        } catch (SQLException e) {
            statementClose();
        }
        try {
            this.resultSet = this.statement.executeQuery("SELECT * FROM " + where);
        } catch (SQLException e) {
            resultSetClose();
        }
    }
    protected void setPreparedStatementBySql(String where) {
        try {
            this.preparedStatement = this.connection.prepareStatement(where);
        } catch (SQLException e) {
            preparedStatementClose();
        }
    }
}