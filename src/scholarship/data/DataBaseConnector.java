package scholarship.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnector {
    private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String UNICODE = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/students" + UNICODE;
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    private Connection connection;

    public static DataBaseConnector instance;
    public static DataBaseConnector getInstance() {
        if(instance == null) {
            instance = new DataBaseConnector();
        }
        return instance;
    }

    public Connection getConnection() {
        try {
            Class.forName(DB_DRIVER);
            return this.connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            closeConnection(this.connection);
        }
        return null;
    }

    public void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}