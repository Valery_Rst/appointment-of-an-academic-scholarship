package scholarship.data;

import scholarship.model.Scholarship;
import scholarship.model.Student;

import java.sql.SQLException;

public class ScholarshipTable extends AbstractTable {
    private static final String TABLE_NAME = "scholarship";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_STUDENT_ID = "student_id";
    private static final String COLUMN_SCHOLARSHIP = "amount";

    public ScholarshipTable() {
        super();
    }

    public Scholarship getScholarshipById(Student student) {
        String sql = TABLE_NAME + " WHERE " + COLUMN_STUDENT_ID + " = ";
        try {
            super.setResultSetBySql(sql + student.getId());
            super.resultSet.next();
            return new Scholarship(
                    super.resultSet.getInt(COLUMN_ID),
                    super.resultSet.getInt(COLUMN_STUDENT_ID),
                    super.resultSet.getInt(COLUMN_SCHOLARSHIP));
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.statementClose();
            super.resultSetClose();
        }
        return null;
    }

    public void assignScholarship(Student student) {
        new StudentDataTable().activeScholarship(student);

        String sql = "UPDATE " + TABLE_NAME + " SET " + COLUMN_SCHOLARSHIP + " =? WHERE " + COLUMN_STUDENT_ID + " =?";
        super.setPreparedStatementBySql(sql);
        try {
            super.preparedStatement.setInt(1, 750);
            super.preparedStatement.setInt(2, student.getId());
            super.preparedStatement.execute();
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            preparedStatementClose();
        }
    }
}