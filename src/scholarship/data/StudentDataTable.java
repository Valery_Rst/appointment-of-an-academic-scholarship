package scholarship.data;

import scholarship.model.Student;
import scholarship.model.StudentData;

import java.sql.SQLException;

public class StudentDataTable extends AbstractTable {
    private static final String TABLE_NAME = "about";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_STUDENT_ID = "student_id";
    private static final String COLUMN_IS_BAD_GRAY = "is_bad_gray";
    private static final String COLUMN_IS_ACTIVE = "is_active_scholarship";

    public StudentData getStudentDataById(Student student) {
        String sql = TABLE_NAME + " WHERE " + COLUMN_STUDENT_ID + " = ";
        try {
            super.setResultSetBySql(sql + student.getId());
            super.resultSet.next();
            return new StudentData(
                    super.resultSet.getInt(COLUMN_ID),
                    super.resultSet.getInt(COLUMN_STUDENT_ID),
                    super.resultSet.getBoolean(COLUMN_IS_BAD_GRAY),
                    super.resultSet.getBoolean(COLUMN_IS_ACTIVE));
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.statementClose();
            super.resultSetClose();
        }
        return null;
    }

    public void activeScholarship(Student student) {
        String sql = "UPDATE " + TABLE_NAME + " SET " + COLUMN_IS_ACTIVE + " =? WHERE " + COLUMN_STUDENT_ID + " =?";
        super.setPreparedStatementBySql(sql);
        try {
            super.preparedStatement.setBoolean(1, true);
            super.preparedStatement.setInt(2, student.getId());
            super.preparedStatement.execute();
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            preparedStatementClose();
        }
    }
}