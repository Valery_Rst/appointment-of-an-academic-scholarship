package scholarship.data;

import scholarship.model.Student;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentTable extends AbstractTable {
    private static final String TABLE_NAME = "student";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_SURNAME = "surname";

    public StudentTable() {
        super();
    }

    public List<Student> getStudentList() {
        List<Student> studentList = new ArrayList<>();
        try {
            super.setResultSetBySql(TABLE_NAME);
            while (super.resultSet.next()) {
                studentList.add(new Student(
                        super.resultSet.getInt(COLUMN_ID),
                        super.resultSet.getString(COLUMN_NAME),
                        super.resultSet.getString(COLUMN_SURNAME)));
            }
            return studentList;
        } catch (SQLException e) {
            super.connectionClose();
        } finally {
            super.statementClose();
            super.resultSetClose();
        }
        return Collections.emptyList();
    }
}