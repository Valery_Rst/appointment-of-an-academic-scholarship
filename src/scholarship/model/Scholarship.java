package scholarship.model;

public class Scholarship {
    private int id, studentId, scholarship;

    public Scholarship(int id, int studentId, int scholarship){
        this.id = id;
        this.studentId = studentId;
        this.scholarship = scholarship;
    }

    public void setScholarship(int scholarship) {
        this.scholarship = scholarship;
    }
    public int getId() {
        return this.id;
    }
    public int getStudentId() {
        return this.studentId;
    }
    public int getScholarship() {
        return this.scholarship;
    }

    @Override
    public String toString() {
        return String.format("\r\tСтипендия состовляет: %dр. ", scholarship);
    }
}