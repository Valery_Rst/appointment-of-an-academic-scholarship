package scholarship.model;

public class Student {
    private int id;
    private String name, surname;
    private Scholarship scholarship;
    private StudentData studentData;

    public Student(int id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setScholarship(Scholarship scholarship) {
        this.scholarship = scholarship;
    }
    public void setStudentData(StudentData studentData) {
        this.studentData = studentData;
    }
    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public Scholarship getScholarship() {
        return this.scholarship;
    }
    public StudentData getStudentData() {
        return this.studentData;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", name, surname, getTextAssignedScholarship(), getTextAssignedData());
    }

    private String getTextAssignedScholarship() {
        return (scholarship != null) ? scholarship.toString() : "\r\tНе определено!";
    }
    private String getTextAssignedData() {
        return (studentData != null) ? studentData.toString() : "\r\tНет информации!";
    }
}