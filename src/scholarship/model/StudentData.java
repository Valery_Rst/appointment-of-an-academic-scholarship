package scholarship.model;

public class StudentData {
    private boolean isActiveScholarship, isBadGray;
    private int id, studentId;

    public StudentData(int id, int studentId, boolean isBadGray, boolean isActiveScholarship){
        this.id = id;
        this.studentId = studentId;
        this.isBadGray = isBadGray;
        this.isActiveScholarship = isActiveScholarship;
    }

    public void setBadGray(boolean badGray) {
        this.isBadGray = badGray;
    }
    public void setActiveScholarship(boolean activeScholarship) {
        this.isActiveScholarship = activeScholarship;
    }
    public int getId() {
        return this.id;
    }
    public int getStudentId() {
        return this.studentId;
    }
    public boolean isBadGray() {
        return this.isBadGray;
    }
    public boolean isActiveScholarship() {
        return this.isActiveScholarship;
    }

    @Override
    public String toString() {
        return String.format("\r\tНеудовлетворительные для назначения стипендии оценки: %s, стипендия назначена: %s" +
                            "\r\t%s", getTextIsBadGray(), getTextIsActive(), getTextAssigned());
    }

    private String getTextIsBadGray() {
        return (this.isBadGray) ? "да" : "нет";
    }
    private String getTextIsActive() {
        return (this.isActiveScholarship) ? "да" : "нет";
    }
    private String getTextAssigned() {
        return (isActiveScholarship) ? "Стипендия назначена!" : getTextVariousAssign();
    }
    private String getTextVariousAssign() {
        return (!isBadGray && !isActiveScholarship) ? "Назначте стипендию!" : "Стипендия не может быть назначена!";
    }
}