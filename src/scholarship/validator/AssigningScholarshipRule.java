package scholarship.validator;

import scholarship.model.Student;

public class AssigningScholarshipRule {
    private Student student;

    public AssigningScholarshipRule(Student student) {
        this.student = student;
    }

    public String getInvalidText() {
        if (isNullInfoAboutScholarship()) {
            return "Нет информации о студенте!";
        } else if (isActiveScholarship()) {
            return "Стипендия уже назначена!";
        } else if (isBadGray()) {
            return "Неудовлетворительные для стипендии оценки!";
        }
        return "";
    }

    private boolean isActiveScholarship() {
        return this.student.getStudentData().isActiveScholarship();
    }
    private boolean isNullInfoAboutScholarship() {
        return this.student.getStudentData() == null;
    }
    private boolean isBadGray() {
        return this.student.getStudentData().isBadGray();
    }
}